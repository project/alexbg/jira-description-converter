﻿using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JiraDataModel.Tests {
    static class TextExt {
        public static IEnumerable<string> ReadAllLines(this Stream stream) {
            using(var reader = new StreamReader(stream)) {
                while(!reader.EndOfStream)
                    yield return reader.ReadLine();
            }
        }
    }

    [TestFixture]
    public class JiraDataModelTests {
        [Test]
        public void LoadNullStringAsTable() {
            Assert.False(JiraTable.TryParse(null, out var table));
        }
        [Test]
        public void LoadEmptyStringAsTable() {
            Assert.False(JiraTable.TryParse(string.Empty, out var table));
        }
        [Test]
        public void LoadString_IsNotATable1() {
            Assert.Throws<JsonReaderException>(
                ()=> JiraTable.TryParse("{adf:display=block}", out var table)
            );
        }
        [Test]
        public void LoadString_IsNotATable2() {
            Assert.Throws<JsonSerializationException>(
                () => JiraTable.TryParse("{'type':'not_a_table'}", out var table)
            );
        }
        [Test]
        public void LoadString_Table() {
            Assert.True(JiraTable.TryParse("{'type':'table'}", out var table));
        }
        [Test]
        public void LoadString_TableContent() {
            using(var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("JiraDataModel.Tests.JiraJsonTable.txt")) {
                var json = stream.ReadAllLines().Skip(1).First();
                json = JiraDescription.TrimAdf(json);
                Assert.True(JiraTable.TryParse(json, out var table));
            }
        }
    }
}
