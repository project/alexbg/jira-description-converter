﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraDataModel.Tests {
    [TestFixture]
    public class JiraTableToMarkdownTests {
        [Test]
        public void ReplaceSlashes() {
            #region var test = @"
            var test = @"
                {
                    'type':'table',
                    'attrs':\{""isNumberColumnEnabled"":false},
                    'content': [
                        {
                            'type':'tableRow',
                            'content': [
                                {
                                    'type':'tableCell',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Cell text'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("|Cell text|\r\n", result);
        }
        [Test]
        public void TableHeaderParagraphText() {
            #region var test = @"
            var test = @"
                {
                    'type':'table',
                    'content': [
                        {
                            'type':'tableHeader',
                            'content': [
                                {
                                    'type':'paragraph',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Header'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("||Header||", result);
        }
        [Test]
        public void TableHeaderTwoParagraphsText() {
            #region var test = @"
            var test = @"
                {
                    'type':'table',
                    'content': [
                        {
                            'type':'tableHeader',
                            'content': [
                                {
                                    'type':'paragraph',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Header1'
                                        }
                                    ]
                                },
                                {
                                    'type':'paragraph',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Header2'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("||Header1\r\nHeader2||", result);
        }
        [Test]
        public void TableHeaderText() {
            #region var test = @"
            var test = @"
                {
                    'type':'table',
                    'content': [
                        {
                            'type':'tableHeader',
                            'content': [
                                {
                                    'type':'text',
                                    'text':'Header'
                                }
                            ]
                        }
                    ]
                }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("||Header||", result);
        }
        [Test]
        public void TableRowCellText() {
            #region var test = @"
            var test = @"
                {
                    'type':'table',
                    'content': [
                        {
                            'type':'tableRow',
                            'content': [
                                {
                                    'type':'tableCell',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Cell text'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("|Cell text|\r\n", result);
        }
        [Test]
        public void TableDifferentRowCellsCount() {
            #region var test = @"
            var test = @"
                {
                    'type':'table',
                    'content': [
                        {
                            'type':'tableRow',
                            'content': [
                                {
                                    'type':'tableCell',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Cell1'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            'type':'tableRow',
                            'content': [
                                {
                                    'type':'tableCell',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Cell2'
                                        }
                                    ]
                                },
                                {
                                    'type':'tableCell',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Cell3'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("|Cell1| |\r\n|Cell2|Cell3|\r\n", result);
        }
        [Test]
        public void TableDifferentRowCellsCountWithHeader() {
            #region var test = @"
            var test = @"
                {
                    'type':'table',
                    'content': [
                        {
                            'type':'tableRow',
                            'content': [
                                {
                                    'type':'tableHeader',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Header'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            'type':'tableRow',
                            'content': [
                                {
                                    'type':'tableCell',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Cell2'
                                        }
                                    ]
                                },
                                {
                                    'type':'tableCell',
                                    'content': [
                                        {
                                            'type':'text',
                                            'text':'Cell3'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("||Header||| |\r\n|Cell2|Cell3|\r\n", result);
        }
        [Test]
        public void TableCellWithHardBreak() {
            #region var test = @"
            var test = @"
            {
                'type':'table',
                'content': [
                    {
                        'type':'tableRow',
                        'content': [
                            {
                                'type':'tableCell',
                                'content': [
                                    {
                                        'type':'text',
                                        'text':'Hard'
                                    },
                                    { 'type':'hardBreak' },
                                    {
                                        'type':'text',
                                        'text':'Break'
                                    },
                                ]
                            }
                        ]
                    },
                ]
            }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("|Hard\\\\ Break|\r\n", result);
        }
        [Test]
        public void TableCellWithMention() {
            #region var test = @"
            var test = @"
            {
                'type':'table',
                'content': [
                    {
                        'type':'tableRow',
                        'content': [
                            {
                                'type':'tableCell',
                                'content': [
                                    {
                                        'type': 'mention',
                                        'attrs': {
                                            'id': '627378c3e01c14006a529d5a',
                                            'text': '',
                                            'accessLevel': ''
                                        }   
                                    }
                                ]
                            }
                        ]
                    },
                ]
            }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("|[mention:627378c3e01c14006a529d5a]|\r\n", result);
        }
        [Test]
        public void TableCellWithNestedBulletLists() {
            #region var test = @"
            var test = @"
{
    'type': 'table',
    'content': [
        {
            'type': 'tableRow',
            'content': [
                {
                    'type': 'tableCell',
                    'attrs': {
                        'colwidth': [
                            529
                        ]
                    },
                    'content': [
                        {
                            'type': 'paragraph',
                            'content': [
                                {
                                    'type': 'text',
                                    'text': 'TEXT 1'
                                }
                            ]
                        },
                        {
                            'type': 'bulletList',
                            'content': [
                                {
                                    'type': 'listItem',
                                    'content': [
                                        {
                                            'type': 'paragraph',
                                            'content': [
                                                {
                                                    'type': 'text',
                                                    'text': 'Item 1'
                                                }
                                            ]
                                        },
                                        {
                                            'type': 'bulletList',
                                            'content': [
                                                {
                                                    'type': 'listItem',
                                                    'content': [
                                                        {
                                                            'type': 'paragraph',
                                                            'content': [
                                                                {
                                                                    'type': 'text',
                                                                    'text': 'Item 2'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    'type': 'listItem',
                                                    'content': [
                                                        {
                                                            'type': 'paragraph',
                                                            'content': [
                                                                {
                                                                    'type': 'text',
                                                                    'text': 'Item 3'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    'type': 'listItem',
                                                    'content': [
                                                        {
                                                            'type': 'paragraph',
                                                            'content': [
                                                                {
                                                                    'type': 'text',
                                                                    'text': 'Item 4'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    'type': 'listItem',
                                    'content': [
                                        {
                                            'type': 'paragraph',
                                            'content': [
                                                {
                                                    'type': 'text',
                                                    'text': 'Item 5'
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    'type': 'listItem',
                                    'content': [
                                        {
                                            'type': 'paragraph',
                                            'content': [
                                                {
                                                    'type': 'text',
                                                    'text': 'Item 6'
                                                }
                                            ]
                                        },
                                        {
                                            'type': 'bulletList',
                                            'content': [
                                                {
                                                    'type': 'listItem',
                                                    'content': [
                                                        {
                                                            'type': 'paragraph',
                                                            'content': [
                                                                {
                                                                    'type': 'text',
                                                                    'text': 'Item 7'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    'type': 'listItem',
                                                    'content': [
                                                        {
                                                            'type': 'paragraph',
                                                            'content': [
                                                                {
                                                                    'type': 'text',
                                                                    'text': 'Item 8'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    'type': 'listItem',
                                                    'content': [
                                                        {
                                                            'type': 'paragraph',
                                                            'content': [
                                                                {
                                                                    'type': 'text',
                                                                    'text': 'Item 9'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("|TEXT 1\r\n* Item 1\r\n** Item 2\r\n** Item 3\r\n** Item 4\r\n* Item 5\r\n* Item 6\r\n** Item 7\r\n** Item 8\r\n** Item 9|\r\n", result);
        }
        [Test]
        public void TableCellWithFormattedText() {
            #region var test = @"
            var test = @"
            {
                'type':'table',
                'content': [
                    {
                        'type':'tableRow',
                        'content': [
                            {
                                'type':'tableCell',
                                'content': [
                                    {
                                        'type': 'text',
                                        'text': 'которые ',
                                        'marks': [
                                            {
                                                'type': 'strong'
                                            }
                                        ] 
                                    }
                                ]
                            }
                        ]
                    },
                ]
            }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("|*которые* |\r\n", result);
        }
        [TestCase("info", "#deebff")]
        [TestCase("note", "#eae6ff")]
        [TestCase("warning", "#fefae6")]
        [TestCase("success", "#e3fcef")]
        [TestCase("error", "#ffebe6")]
        public void TableCellWithPanel(string panelType, string color) {
            #region var test = @"
            var test = @"
            {{
                'type':'table',
                'content': [
                    {{
                        'type':'tableRow',
                        'content': [
                            {{
                                'type':'tableCell',
                                'content': [
                                    {{
                                        'type': 'panel',
                                        'attrs': {{
                                            'panelType': '{0}'
                                        }},
                                        'content': [
                                            {{
                                                'type': 'paragraph',
                                                'content': [
                                                    {{
                                                        'type': 'text',
                                                        'text': 'Panel content'
                                                    }}
                                                ]
                                            }}
                                        ]
                                    }}
                                ]
                            }}
                        ]
                    }},
                ]
            }}
            ";
            #endregion 
            test = string.Format(test, panelType);
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            var expected = $"|{{panel:bgColor={color}}}Panel content{{panel}}|\r\n";
            Assert.AreEqual(expected, result);
        }
        [TestCase("green")]
        public void TableCellWithStatus(string color) {
            #region var test = @"
            var test = @"
            {{
                'type':'table',
                'content': [
                    {{
                        'type':'tableRow',
                        'content': [
                            {{
                                'type':'tableCell',
                                'content': [
                                    {{
                                        'type': 'status',
                                        'attrs': {{
                                            'text': 'DONE',
                                            'color': '{0}',
                                            'localId': 'ec43e460-8b42-459e-b73b-18c2f7f20d25',
                                            'style': ''
                                        }}
                                    }}
                                ]
                            }}
                        ]
                    }},
                ]
            }}
            ";
            #endregion 
            test = string.Format(test, color);
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            var expected = $"|{{panel:bgColor={color}}}DONE{{panel}}|\r\n";
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void TableCellWithoutContent() {
            #region var test = @"
            var test = @"
            {
                'type':'table',
                'content': [
                    {
                        'type':'tableRow',
                        'content': [
                            {
                                'type': 'tableCell',
                                'attrs': {
                                    'colwidth': [110]
                                },
                                'content': [
                                    {
                                        'type': 'paragraph',
                                        'content': []
                                    }
                                ]
                            },
                        ]
                    },
                ]
            }
            ";
            #endregion 
            Assert.True(JiraTable.TryParse(test, out var table));

            var result = table.ToMarkdown();
            Assert.AreEqual("| |\r\n", result);
        }
    }
}
