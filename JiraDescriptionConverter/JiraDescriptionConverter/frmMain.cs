﻿using JiraDataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JiraDescriptionConverter {
    public partial class frmMain:Form {
        public frmMain() {
            InitializeComponent();
        }

        private void btnCopyMarkdown_Click(object sender, EventArgs e) {
            var sb = new StringBuilder();
            var lines = tbxMarkdown.Lines;
            foreach(var str in lines)
                sb.AppendLine(str);

            if(sb.Length <= 0)
                return;

            Clipboard.SetText(sb.ToString());
            MessageBox.Show("Скопировано в буфер обмена...");
        }

        private void tbxJson_TextChanged(object sender, EventArgs e) {
            tbxMarkdown.Clear();
            tbxErrors.Clear();

            var description = new JiraDescription(tbxJson.Lines);
            description.OnError += (ex) => {
                tbxErrors.Text += "ERROR: " + ex.Message + "\r\n\r\n";
            };
            tbxMarkdown.Lines = description.ConvertTablesToMarkdown().ToArray();
        }

        private void btnClear_Click(object sender, EventArgs e) {
            tbxJson.Clear();
            tbxMarkdown.Clear();
        }

        private void btnConvert_Click(object sender, EventArgs e) {
            tbxJson_TextChanged(sender, e);
        }
    }
}
