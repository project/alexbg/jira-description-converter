﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraDataModel {
    public class JiraDescription {
        readonly string[] textLines;
        public JiraDescription(string[] textLines) {
            this.textLines = TrimAdf(textLines).ToArray();
        }

        static IEnumerable<string> TrimAdf(string[] textLines) {
            foreach(var str in textLines)
                yield return TrimAdf(str);
        }
        public static string TrimAdf(string json) {
            const string adf1 = "{adf}";
            const string adf2 = "{adf:display=block}";

            json = json.Trim();

            if(json.StartsWith(adf1))
                return TrimAdf(json.Substring(adf1.Length));

            if(json.StartsWith(adf2))
                return TrimAdf(json.Substring(adf2.Length));

            if(json.EndsWith(adf1))
                return TrimAdf(json.Substring(0, json.Length - adf1.Length));

            if(json.EndsWith(adf2))
                return TrimAdf(json.Substring(0, json.Length - adf2.Length));

            return json;
        }

        public event Action<Exception> OnError;

        string ConvertLine(string str) {
            try {
                if(str.StartsWith("{")) {
                    var trimmedStr = TrimAdf(str);
                    if(JiraTable.TryParse(trimmedStr, out var table))
                        return table.ToMarkdown();

                    return trimmedStr;
                } else return str;
            } catch(Exception ex) {
                OnError(ex);
                return str;
            }
        }
        public IEnumerable<string> ConvertTablesToMarkdown() {
            foreach(var str in textLines)
                yield return ConvertLine(str);
        }
    }

    public class JiraObjectConverter : JsonConverter {
        public override bool CanWrite { get { return false; } }
        public override bool CanConvert(Type objectType) {
            return typeof(JiraObject).IsAssignableFrom(objectType);
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) {
            var token = JToken.Load(reader);
            var typeToken = token["type"];
            if(typeToken == null)
                throw new InvalidOperationException("invalid object");

            var actualType = JiraObject.GetType(typeToken.ToObject<JiraObjectType>(serializer));
            if(existingValue == null || existingValue.GetType() != actualType) {
                var contract = serializer.ContractResolver.ResolveContract(actualType);
                existingValue = contract.DefaultCreator();
            }
            using(var subReader = token.CreateReader()) {
                serializer.Populate(subReader, existingValue);
            }
            return existingValue;
        }
    }

    public enum JiraObjectType {
        objectType,
        table,
        tableRow,
        tableHeader,
        paragraph,
        text,
        tableCell,
        mention,
        hardBreak,
        bulletList,
        orderedList,
        listItem,
        mediaGroup,
        media,
        inlineCard,
        panel,
        status,
    }

    [JsonConverter(typeof(JiraObjectConverter))]
    public class JiraObject {
        static readonly Dictionary<Type, JiraObjectType> typeToSubType;
        static readonly Dictionary<JiraObjectType, Type> subTypeToType;

        static JiraObject() {
            typeToSubType = new Dictionary<Type, JiraObjectType>()  {
                { typeof(JiraObject), JiraObjectType.objectType },
                { typeof(JiraTable), JiraObjectType.table },
                { typeof(JiraTableRow), JiraObjectType.tableRow },
                { typeof(JiraTableHeader), JiraObjectType.tableHeader },
                { typeof(JiraParagraph), JiraObjectType.paragraph },
                { typeof(JiraText), JiraObjectType.text },
                { typeof(JiraTableCell), JiraObjectType.tableCell },
                { typeof(JiraMention), JiraObjectType.mention },
                { typeof(JiraHardBreak), JiraObjectType.hardBreak },
                { typeof(JiraBulletList), JiraObjectType.bulletList },
                { typeof(JiraOrderedList), JiraObjectType.orderedList },
                { typeof(JiraListItem), JiraObjectType.listItem },
                { typeof(JiraMediaGroup), JiraObjectType.mediaGroup },
                { typeof(JiraMedia), JiraObjectType.media },
                { typeof(JiraInlineCard), JiraObjectType.inlineCard },
                { typeof(JiraPanel), JiraObjectType.panel },
                { typeof(JiraStatus), JiraObjectType.status },
            };
            subTypeToType = typeToSubType.ToDictionary(pair => pair.Value, pair => pair.Key);
        }

        public static Type GetType(JiraObjectType subType) {
            return subTypeToType[subType];
        }

        //[JsonConverter(typeof(StringEnumConverter))]
        //public JiraObjectType Type { get { return typeToSubType[GetType()]; } }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("type")] public JiraObjectType ElementType { get; set; }
    }
    interface IHasContent {
        JiraObject[] Content { get; set; }
    }
    interface IJiraList { }

    public class JiraTableRow : JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraTableHeader:JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraParagraph:JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraTableCell:JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraBulletList:JiraObject, IHasContent, IJiraList {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraOrderedList:JiraObject, IHasContent, IJiraList {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraListItem:JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraMediaGroup:JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
    }
    public class JiraMedia:JiraObject {
    }

    public enum JiraTextMarkType {
        code,
        em,
        link,
        strike,
        strong,
        subsup,
        textColor,
        underline
    }
    public enum JiraSubSupType {
       sub, sup
    }
    public class JiraTextMarkAttributes {
        [JsonProperty("href")] public string Href { get; set; }
        [JsonProperty("title")] public string Title { get; set; }
        [JsonProperty("color")] public string Color { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("type")] public JiraSubSupType SubSupType { get; set; }
    }
    public class JiraTextMark {
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("type")] public JiraTextMarkType Type { get; set; }
        [JsonProperty("attrs")] public JiraTextMarkAttributes Attributes { get; set; }        
    }
    public class JiraText: JiraObject {
        [JsonProperty("text")] public string Text { get; set; }
        [JsonProperty("marks")] public JiraTextMark[] Marks { get; set; }
    }
    public class JiraInlineCardAttributes {
        [JsonProperty("url")] public string URL { get; set; }
    }
    public class JiraInlineCard:JiraObject {
        [JsonProperty("attrs")] public JiraInlineCardAttributes Attributes { get; set; }
    }
    public class JiraHardBreak:JiraObject {        
    }

    public class JiraMentionAttributes {
        [JsonProperty("id")] public string ID { get; set; }
        [JsonProperty("text")] public string Text { get; set; }
        [JsonProperty("accessLevel")] public string AccessLevel { get; set; }
    }
    public class JiraMention:JiraObject {
        [JsonProperty("attrs")] public JiraMentionAttributes Attributes { get; set; }
    }
    public enum JiraPanelType {
        info, note, warning, success, error
    }
    public class JiraPanelAttributes {
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("panelType")] public JiraPanelType Type { get; set; }
    }
    public class JiraPanel:JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
        [JsonProperty("attrs")] public JiraPanelAttributes Attributes { get; set; }
    }

    public class JiraStatusAttributes {
        [JsonProperty("text")] public string Text { get; set; }
        [JsonProperty("color")] public string Color { get; set; }
    }
    public class JiraStatus: JiraObject {
        [JsonProperty("attrs")] public JiraStatusAttributes Attributes { get; set; }
    }

    public class JiraTable :JiraObject, IHasContent {
        [JsonProperty("content")] public JiraObject[] Content { get; set; }
        static string ReplaceSlashes(string json) {
            const string pattern = "\\{\"";
            const string newValue = "{\"";
            return json.Replace(pattern, newValue);
        }
        public static bool TryParse(string json, out JiraTable table) {
            if(string.IsNullOrEmpty(json)) {
                table = null;
                return false;
            }

            json = ReplaceSlashes(json);
            table = JsonConvert.DeserializeObject<JiraTable>(json);
            return table != null && table.ElementType == JiraObjectType.table;
        }

        public string ToMarkdown() {
            var converter = new JiraDescriptionToMarkdownConverter();
            return converter.Convert(this);
        }
    }
}
