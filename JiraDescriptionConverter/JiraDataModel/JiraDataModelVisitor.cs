﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraDataModel {
    public class JiraDataModelContext {
        public JiraDataModelContext(JiraDataModelContext parent, JiraObject obj) {
            Parent = parent;
            Object = obj;

            ListEmbeddingLevel = parent != null ? parent.ListEmbeddingLevel : 0;
            var list = Object as IJiraList;
            if(list != null)
                ListEmbeddingLevel += 1;
        }
        public readonly JiraDataModelContext Parent;
        public readonly JiraObject Object;

        int? tableColumnCount;
        public int GetTableColumnCount() {
            if(tableColumnCount.HasValue)
                return tableColumnCount.Value;

            var table = Object as JiraTable;
            if(table != null) {
                tableColumnCount = table.Content.Cast<JiraTableRow>()
                        .Select(r => r.Content.Length).Max();
                return tableColumnCount.Value;
            }

            var count = Parent?.GetTableColumnCount();
            tableColumnCount = count.HasValue ? count.Value : 0;
            return tableColumnCount.Value;
        }
        public int ListEmbeddingLevel { get; private set; }
    }

    public class JiraDataModelVisitor {
        public virtual void Accept(JiraObject obj) {
            var context = new JiraDataModelContext(null, obj);
            VisitObject(context);
            VisitContent(context);
            CloseObject(context);
        }
        void VisitObject(JiraDataModelContext context) {
            var obj = context.Object;
            var type = obj.GetType();
            if(type == typeof(JiraTable))
                Visit((JiraTable)obj, context);

            else if(type == typeof(JiraTableRow))
                Visit((JiraTableRow)obj, context);

            else if(type == typeof(JiraTableHeader))
                Visit((JiraTableHeader)obj, context);

            else if(type == typeof(JiraParagraph))
                Visit((JiraParagraph)obj, context);

            else if(type == typeof(JiraTableCell))
                Visit((JiraTableCell)obj, context);

            else if(type == typeof(JiraText))
                Visit((JiraText)obj, context);

            else if(type == typeof(JiraMention))
                Visit((JiraMention)obj, context);

            else if(type == typeof(JiraHardBreak))
                Visit((JiraHardBreak)obj, context);

            else if(type == typeof(JiraBulletList))
                Visit((JiraBulletList)obj, context);

            else if(type == typeof(JiraListItem))
                Visit((JiraListItem)obj, context);

            else if(type == typeof(JiraInlineCard))
                Visit((JiraInlineCard)obj, context);

            else if(type == typeof(JiraPanel))
                Visit((JiraPanel)obj, context);

            else if(type == typeof(JiraStatus))
                Visit((JiraStatus)obj, context);
        }
        void CloseObject(JiraDataModelContext context) {
            var obj = context.Object;
            var type = obj.GetType();
            if(type == typeof(JiraTable))
                Close((JiraTable)obj, context);

            if(type == typeof(JiraTableRow))
                Close((JiraTableRow)obj, context);

            else if(type == typeof(JiraTableHeader))
                Close((JiraTableHeader)obj, context);

            else if(type == typeof(JiraParagraph))
                Close((JiraParagraph)obj, context);

            else if(type == typeof(JiraTableCell))
                Close((JiraTableCell)obj, context);

            else if(type == typeof(JiraText))
                Close((JiraText)obj, context);

            else if(type == typeof(JiraMention))
                Close((JiraMention)obj, context);

            else if(type == typeof(JiraHardBreak))
                Close((JiraHardBreak)obj, context);

            else if(type == typeof(JiraBulletList))
                Close((JiraBulletList)obj, context);

            else if(type == typeof(JiraListItem))
                Close((JiraListItem)obj, context);

            else if(type == typeof(JiraInlineCard))
                Close((JiraInlineCard)obj, context);

            else if(type == typeof(JiraPanel))
                Close((JiraPanel)obj, context);

            else if(type == typeof(JiraStatus))
                Close((JiraStatus)obj, context);
        }
        void VisitContent(JiraDataModelContext context) {
            var hasContent = context?.Object as IHasContent;
            if(hasContent == null || hasContent.Content == null)
                return;
            foreach(var obj in hasContent.Content) {
                var childContext = new JiraDataModelContext(context, obj);
                VisitObject(childContext);
                VisitContent(childContext);
                CloseObject(childContext);
            }
        }

        protected virtual void Visit(JiraTable table, JiraDataModelContext context) { }
        protected virtual void Visit(JiraTableRow tableRow, JiraDataModelContext context) { }
        protected virtual void Visit(JiraTableHeader tableHeader, JiraDataModelContext context) { }
        protected virtual void Visit(JiraParagraph paragraph, JiraDataModelContext context) { }
        protected virtual void Visit(JiraText text, JiraDataModelContext context) { }
        protected virtual void Visit(JiraTableCell cell, JiraDataModelContext context) { }
        protected virtual void Visit(JiraMention mention, JiraDataModelContext context) { }
        protected virtual void Visit(JiraHardBreak hardBreak, JiraDataModelContext context) { }
        protected virtual void Visit(JiraBulletList bulletList, JiraDataModelContext context) { }
        protected virtual void Visit(JiraListItem listItem, JiraDataModelContext context) { }
        protected virtual void Visit(JiraInlineCard inlineCard, JiraDataModelContext context) { }
        protected virtual void Visit(JiraPanel panel, JiraDataModelContext context) { }
        protected virtual void Visit(JiraStatus status, JiraDataModelContext context) { }

        protected virtual void Close(JiraTableRow tableRow, JiraDataModelContext context) { }
        protected virtual void Close(JiraTableHeader tableHeader, JiraDataModelContext context) { }
        protected virtual void Close(JiraParagraph paragraph, JiraDataModelContext context) { }
        protected virtual void Close(JiraText text, JiraDataModelContext context) { }
        protected virtual void Close(JiraTableCell cell, JiraDataModelContext context) { }
        protected virtual void Close(JiraMention mention, JiraDataModelContext context) { }
        protected virtual void Close(JiraHardBreak hardBreak, JiraDataModelContext context) { }
        protected virtual void Close(JiraTable table, JiraDataModelContext context) { }
        protected virtual void Close(JiraBulletList bulletList, JiraDataModelContext context) { }
        protected virtual void Close(JiraListItem listItem, JiraDataModelContext context) { }
        protected virtual void Close(JiraInlineCard inlineCard, JiraDataModelContext context) { }
        protected virtual void Close(JiraPanel panel, JiraDataModelContext context) { }
        protected virtual void Close(JiraStatus status, JiraDataModelContext context) { }
    }

    public class JiraDescriptionToMarkdownConverter:JiraDataModelVisitor {
        StringBuilder sb = new StringBuilder();
        public string Convert(JiraObject obj) {
            sb.Clear();
            Accept(obj);
            return sb.ToString();
        }
        protected override void Visit(JiraTableHeader tableHeader, JiraDataModelContext context) {
            sb.Append("||");
        }
        protected override void Close(JiraTableHeader tableHeader, JiraDataModelContext context) {
            sb.Append("||");
        }

        void InsertLeadingWS(JiraText text) {
            var count = text.Text.Length - text.Text.TrimStart().Length;
            sb.Append(' ', count);
        }
        void InsertTrailingWS(JiraText text) {
            var count = text.Text.Length - text.Text.TrimEnd().Length;
            sb.Append(' ', count);
        }
        protected override void Visit(JiraText text, JiraDataModelContext context) {
            if(text.Marks != null) {
                InsertLeadingWS(text);

                foreach(var mark in text.Marks) {
                    switch(mark.Type) {
                        case JiraTextMarkType.code: sb.Append("{{"); break;
                        case JiraTextMarkType.em: sb.Append("_"); break;
                        case JiraTextMarkType.link:
                            sb.Append($"[{mark.Attributes.Title}|{mark.Attributes.Href}]");
                            break;
                        case JiraTextMarkType.strike: sb.Append("-"); break;
                        case JiraTextMarkType.strong: sb.Append("*"); break;
                        case JiraTextMarkType.subsup:
                            if(mark.Attributes.SubSupType == JiraSubSupType.sub)
                                sb.Append("~");
                            else if(mark.Attributes.SubSupType == JiraSubSupType.sup)
                                sb.Append("^");
                            break;
                        case JiraTextMarkType.textColor:
                            sb.Append($"{{color:{mark.Attributes.Color}}}");
                            break;
                        case JiraTextMarkType.underline: sb.Append("+"); break;
                    }
                }
                sb.Append(text.Text.Trim());
            }
            else
                sb.Append(text.Text);
        }
        protected override void Close(JiraText text, JiraDataModelContext context) {
            if(text.Marks == null)
                return;
                
            foreach(var mark in text.Marks) {
                switch(mark.Type) {
                    case JiraTextMarkType.code: sb.Append("}}"); break;
                    case JiraTextMarkType.em: sb.Append("_"); break;
                    case JiraTextMarkType.link:
                        break;
                    case JiraTextMarkType.strike: sb.Append("-"); break;
                    case JiraTextMarkType.strong: sb.Append("*"); break;
                    case JiraTextMarkType.subsup:
                        if(mark.Attributes.SubSupType == JiraSubSupType.sub)
                            sb.Append("~");
                        else if(mark.Attributes.SubSupType == JiraSubSupType.sup)
                            sb.Append("^");
                        break;
                    case JiraTextMarkType.textColor:
                        sb.Append($"{{color}}");
                        break;
                    case JiraTextMarkType.underline: sb.Append("+"); break;
                }
            }

            InsertTrailingWS(text);
        }
        protected override void Close(JiraParagraph paragraph, JiraDataModelContext context) {
            var parent = context?.Parent.Object as IHasContent;
            if(parent != null && parent is JiraTableCell && parent.Content.Length == 1
            && (paragraph.Content == null || paragraph.Content.Length == 0)) {
                sb.Append(" ");
                return;
            }

            if(parent == null || parent.Content.Last() != paragraph)
                sb.Append(Environment.NewLine);           
        }
        protected override void Visit(JiraTableCell cell, JiraDataModelContext context) {
            sb.Append("|");
        }
        protected override void Close(JiraTableRow tableRow, JiraDataModelContext context) {
            sb.Append('|');

            var parentTableColumnCount = context.GetTableColumnCount();
            var cellsNeeded = parentTableColumnCount - tableRow.Content.Length;
            for(int i = 0; i < cellsNeeded; i++)
                sb.Append(" |");

            sb.Append(Environment.NewLine);
        }
        protected override void Visit(JiraHardBreak hardBreak, JiraDataModelContext context) {
            sb.Append("\\\\ ");
        }
        protected override void Visit(JiraMention mention, JiraDataModelContext context) {
            sb.Append($"[mention:{mention.Attributes.ID}]{mention.Attributes.Text}");
        }
        protected override void Visit(JiraListItem listItem, JiraDataModelContext context) {           
            sb.Append('*', context.ListEmbeddingLevel - 1);
            switch(context.Parent.Object.ElementType) {
                case JiraObjectType.bulletList: sb.Append("* "); break;
                case JiraObjectType.orderedList: sb.Append("# "); break;
            }
        }
        protected override void Close(JiraListItem listItem, JiraDataModelContext context) {
            var parent = context.Parent.Object as IHasContent;
            if(parent == null)
                return;

            if(parent.Content.Last() == listItem)
                return;

            sb.Append(Environment.NewLine);
        }
        protected override void Visit(JiraInlineCard inlineCard, JiraDataModelContext context) {
            sb.Append($"[{inlineCard.Attributes.URL}|{inlineCard.Attributes.URL}]");
        }
        protected override void Visit(JiraPanel panel, JiraDataModelContext context) {
            if(panel.Attributes == null) {
                sb.Append($"{{panel}}");
                return;
            }
            switch(panel.Attributes.Type) {
                case JiraPanelType.error:
                    sb.Append($"{{panel:bgColor=#ffebe6}}");
                    break;
                case JiraPanelType.info:
                    sb.Append($"{{panel:bgColor=#deebff}}");
                    break;
                case JiraPanelType.success:
                    sb.Append($"{{panel:bgColor=#e3fcef}}");
                    break;
                case JiraPanelType.warning:
                    sb.Append($"{{panel:bgColor=#fefae6}}");
                    break;
                case JiraPanelType.note:
                    sb.Append($"{{panel:bgColor=#eae6ff}}");
                    break;
            }
        }
        protected override void Close(JiraPanel panel, JiraDataModelContext context) {
            sb.Append($"{{panel}}");
        }
        protected override void Visit(JiraStatus status, JiraDataModelContext context) {
            if(status.Attributes == null || string.IsNullOrEmpty(status.Attributes.Color)) {
                sb.Append($"{{panel}}");
                return;
            }
            sb.Append($"{{panel:bgColor={status.Attributes.Color}}}");
        }
        protected override void Close(JiraStatus status, JiraDataModelContext context) {
            if(status.Attributes != null && !string.IsNullOrEmpty(status.Attributes.Text))
                sb.Append(status.Attributes.Text);

            sb.Append($"{{panel}}");
        }
    }
}
